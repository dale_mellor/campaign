from campaign import Campaign
import tempfile
import shutil
import os
from contextlib import contextmanager

BASE_DIR = os.path.join(os.path.split(__file__)[0], "..")
EXAMPLE_DIR = os.path.join(BASE_DIR, "examples")

@contextmanager
def create_from_example():
    """We will use this example campaign various times"""
    #Create the campaign
    campaign_dir = tempfile.mkdtemp()
    launcher = None
    c = Campaign.create(campaign_dir, launcher)

    #Read the example yaml file
    filename = os.path.join(EXAMPLE_DIR, "example.yaml")
    c.read(filename)

    #start the "with" block wherever this function is called
    yield c

    #at the end of the "with" block, clean up by removing the
    #campaign directory
    shutil.rmtree(campaign_dir)



def test_example():
    with create_from_example() as c:
        #Check that the file read properly
        assert len(c.runs)==2
        assert c.runs['fid_cfh'].get_value("cosmological_parameters", "tau") == 0.08
        assert c.runs['fid_nu6'].text == "Neutrino mass 0.06"

def test_run_copy():
    with create_from_example() as c:
        c.copy("fid_cfh", "fid_cfh_copy")
        assert "fid_cfh_copy" in c.runs
        assert c.runs['fid_cfh_copy'].get_value("cosmological_parameters", "tau") == 0.08

def test_serialize():
    with create_from_example() as c:
        c.save()
        c2 = Campaign.load(c.campaign_directory)
        assert c2.runs['fid_cfh'].get_value("cosmological_parameters", "tau") == 0.08
        assert c2.runs['fid_nu6'].text == "Neutrino mass 0.06"

def test_next_version():
    with create_from_example() as c:
          c.next_version("fid_cfh")
          assert ("fid_cfh", 1) in c.old_runs
          assert c.runs["fid_cfh"].version == 2


if __name__ == '__main__':
    test_example()