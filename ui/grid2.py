from PyQt5.QtCore import QDate, QDateTime, QRegExp, QSortFilterProxyModel, Qt, QTime, QAbstractItemModel
from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PyQt5.QtWidgets import QApplication, QCheckBox, QComboBox, QGridLayout
from PyQt5.QtWidgets import QGroupBox, QHBoxLayout, QLabel, QLineEdit
from PyQt5.QtWidgets import QTreeView, QVBoxLayout, QWidget, QPushButton
import campaign
from campaign import Campaign, Run
import campaign.status
import campaign.errors
import sys
sys.path.append("./ui/")
import test2_ui




class RunModel(object):
    def __init__(self, run):
        self.run = run

    def adapt(self):
        info = self.run.info()
        status_index = Run.INFO_FIELDS.index("Status")
        info = list(info)
        status = info[status_index]
        info[status_index] = campaign.status.STATUS_ICON[status]
        row = [QStandardItem(unicode(entry)) for entry in info]
        return row



class CampaignWindow(QWidget, test2_ui.Ui_Form):
    def __init__(self, campaign):
        super(CampaignWindow, self).__init__()
        self.campaign = campaign

        self.setupUi(self)

        # self.table_model = CampaignListModel(self.campaign, self)
        self.table_model = QStandardItemModel(0, len(Run.INFO_FIELDS), self)
        self.runTable.setModel(self.table_model)
        self.update_table()

        # Buttons
        self.quitButton.clicked.connect(self.quit)
        self.testButton.clicked.connect(self.test_runs)
        self.resetButton.clicked.connect(self.reset_runs)
        self.invalidateButton.clicked.connect(self.invalidate_runs)
        self.nextButton.clicked.connect(self.next_runs)
        self.topLabel.setText("Runs {}".format(campaign.campaign_directory))



    def update_table(self):
        # self.table_model.clear()
        nrow = self.table_model.rowCount()
        self.table_model.removeRows(0,nrow)
        # Why is this not working???
        for i, param in enumerate(Run.INFO_FIELDS):
            self.table_model.setHeaderData(i, Qt.Horizontal, param)        
        for run in self.campaign.runs.values():
            run_model = RunModel(run) #move these so we have one each
            self.table_model.appendRow(run_model.adapt())
        # self.table_model.select()


    def quit(self):
        QApplication.quit()



    def test_runs(self):
        rows = list(set([index.row() for index in self.runTable.selectedIndexes()]))
        rows.sort()
        for row in rows:
            run = self.campaign.runs.values()[row]
            run.test_synchronously()
        self.update_table()

    def invalidate_runs(self):
        rows = list(set([index.row() for index in self.runTable.selectedIndexes()]))
        rows.sort()
        for row in rows:
            run = self.campaign.runs.values()[row]
            run.invalidate()
        self.update_table()

    def next_runs(self):
        rows = list(set([index.row() for index in self.runTable.selectedIndexes()]))
        rows.sort()
        for row in rows:
            run = self.campaign.runs.values()[row]
            self.campaign.next_version(run.name)
        self.update_table()

    def reset_runs(self):
        rows = list(set([index.row() for index in self.runTable.selectedIndexes()]))
        rows.sort()
        for row in rows:
            run = self.campaign.runs.values()[row]
            run.reset()
        self.update_table()




def main():
    import sys

    app = QApplication(sys.argv)
    campaign=Campaign.load("/Users/jaz/src/campaign/campaigns")
    window = CampaignWindow(campaign)
    window.show()
    status = app.exec_()
    sys.exit(status)


if __name__ == '__main__':
    main()