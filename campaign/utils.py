from cosmosis.runtime.utils import mkdir
import errno
import os
import collections

def mkdir(path):
    #This is much nicer in python 3.
    try:
        os.makedirs(path)
    except OSError as error:
        if error.errno == errno.EEXIST:
            if os.path.isdir(path):
                #error is that dir already exists; fine - no error
                pass
            else:
                #error is that file with name of dir exists already
                raise ValueError("Tried to create dir %s but file with name exists already"%path)
        elif error.errno == errno.ENOTDIR:
            #some part of the path (not the end) already exists as a file 
            raise ValueError("Tried to create dir %s but some part of the path already exists as a file"%path)
        else:
            #Some other kind of error making directory
            raise
def dictify(obj):
    if hasattr(obj, "to_dict"):
        return obj.to_dict()
    out = obj.__dict__.copy()
    for key,val in obj.__dict__.items():
        if hasattr(val,"__dict__"):
            out[key] = dictify(val)
    return out

def line_count(filename):
    n=0
    for line in open(filename):
        n+=1
    return n

def uncommented_line_count(filename):
    n=0
    for line in open(filename):
        if not line.startswith('#'):
            n+=1
    return n


def derialize(filename):
    import yaml
    return yaml.load(open(filename))

def tail(filename, lines):
    #number of bites to try jumping bac 
    chunk_size = 4096


    lines = int(sys.argv[1])
    fname = sys.argv[2]
    fsize = os.stat(fname).st_size

    iter = 0
    with open(sys.argv[2]) as f:
        if bufsize > fsize:
            bufsize = fsize-1
        data = []
        while True:
            iter +=1
            f.seek(fsize-bufsize*iter)
            data.extend(f.readlines())
            if len(data) >= lines or f.tell() == 0:
                return data[-lines:]

class ParseError(Exception):
    pass

def parse_integers(ref):
    vals = ref.split(',')
    vals = [parse_integers_sub(v) for v in vals]
    vals = sum(vals, [])
    vals = list(set(vals))
    vals.sort()
    return vals


def flatten(l):
    if isinstance(l, int):
        return l
    x = []
    for v in l:
        x.append(flatten(v))
    return x

def parse_integers_sub(val):
    if '-' in val:
        parts = val.split('-')
        if len(parts)!=2:
            raise ParseError("Could not parse {} as a range of integers".format(val))
        a, b = parts
        try:
            a = int(a)
            b = int(b)
        except ValueError:
            raise ParseError("Could not parse {} as an integer or range".format(val))
        if b<=a:
            raise ParseError("Could not parse {} as an integer or range; b<a".format(val))
        return range(a, b+1)

    else:
        try:
            val = int(val)
        except ValueError:
            raise ParseError("Could not parse {} as an integer or range".format(val))
        return [val]



def changes_text(changes):
    output = collections.defaultdict(dict)
    for (section, name),value in changes.items():
        output[section][name] = value
    text = []
    for section, contents in output.items():
        text.append("[{}]\n".format(section))
        for name, value in contents.items():
            text.append("{} = {}".format(name, value))
    return text