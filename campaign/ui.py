import os
import sys
import cmd
import argparse
import logging
import collections
import humanize
import datetime
import glob
import readline
from . import jobs
from . import utils
from . import errors
from .campaign import Campaign
from .tabulate import tabulate
from .log import NOTE
from .status import *
from .errors import *
import subprocess

readline.set_completer_delims(' \t\n')



selecting_runs_doc = """

    Runs can be chosen by name:
        my_run_name
    Or by index:
        1
    You can specify more than one run:
        my_run_name 2 3 4
    And you can also specify multiple or ranges of runs:
        2-4,6,7
"""

class CampaignUI(cmd.Cmd):
    def __init__(self, campaign_directory):
        self.campaign_directory = campaign_directory
        cmd.Cmd.__init__(self)


    def _autocomplete_run_name(self, text, line, begidx, endidx):
        return [name for name in self.campaign.run_names() if name.startswith(text)]

    def _autocomplete_path(self, text, line, begidx, endidx):
        pattern = "{}*".format(text)
        f =  glob.glob(pattern)
        return f

    def _autocomplete_files_in(self, dirname, start):
        pattern = "{}/{}*".format(dirname, start)
        f =  glob.glob(pattern)
        n = len(dirname)+1
        f = [g[n:] for g in f]
        return f

    def onecmd(self, *args, **kwargs):
        try:
            return cmd.Cmd.onecmd(self, *args, **kwargs)
        except UIError as error:
            logging.error(error) 

    def print_table(self, data, header=()):
        """Print a table in a UI standard style. Pass in a list of rows and a header list."""
        print tabulate(data, header, tablefmt='fancy_grid', stralign='center', numalign='decimal').encode('utf-8')

    def preloop(self):
        # #choose a directory and create a campaign.
        # if os.path.exists(Campaign.campaign_filename(self.campaign_directory)):
        #     self.campaign = Campaign.load(self.campaign_directory)
        # else:
        self.campaign = Campaign.load (self.campaign_directory)
        
        self.load_history()


    def load_history(self):
        try:
            import readline
            history = self.history_file()
            if os.path.exists(history):
                readline.read_history_file(history)
        except:
            pass

    def save_history(self):
        try:
            import readline
            history = self.history_file()
            history = os.path.join(self.campaign_directory, ".campaign_ui_history")
            readline.write_history_file(history)

        except ImportError:
            pass

    def do_EOF(self, line):
        return True

    def history_file(self):
        return os.path.join(self.campaign_directory, ".campaign_ui_history")

    def postloop(self):
        self.save_history()

    def get_filename(self, line):
        filename = line.strip()
        if not os.path.exists(filename):
            raise UIError("File {} does not exist".format(filename))
        if not os.path.isfile(filename):
            raise UIError("{} is not a file - probably a directory?".format(filename))
        if not os.access(filename, os.R_OK):
            raise UIError("{} is not accessible - probably a permissions problem.".format(filename))
        return filename

    def do_launch_info(self, line):
        "Display the current job launcher. Currently just prints out the instance."
        print self.campaign.runs.values () [0].get_param (
                                               "campaign", "launcher").lower ()


    def parse_runs(self, line):
        names = line.strip().split()
        if not names:
            raise UIError("Specify a run name (type 'list' to get a list of runs)")

        runs = []
        for name in names:
            if name[0] in '0123456789':
                indices = utils.parse_integers(name)
                for index in indices:
                    try:
                        run = self.campaign.runs.values()[index]
                        runs.append(run)
                    except IndexError:
                        raise UIError("Could not get run {}; out of range".format(index))
            else: #run with name
                runs.append(self.parse_run(name))
        return runs


    def parse_run(self, line):
        name = line.strip()
        if name=='':
            raise UIError("Specify a run name (type 'list' to get a list of runs)")
        try:
            run = self.campaign.runs[name]
            return run
        except KeyError:
            raise UIError("Unknown run '{}'".format(name))


    def do_list(self, line):
        """Print a list of all the known runs, displaying various bits of info about them."""
        header = ['', 'Name', 'Version', 'Status', 'Sampler', 'Chain length', 'Outputs', 'Job']
        table = []
        
        i = 1
        for run in self.campaign.runs.values():
            (name, version, status, sampler, lines, n_outputs, job) = run.info()
            table.append ((i, name, version, STATUS_ICON.get (status, status),
                           sampler, lines, n_outputs, job))
            i = i+1
        self.print_table(table, header)

    def do_log(self, line):
        """Print out the log file from a run, if the job has been run."""
        run = self.parse_run(line)
        path = run.log_filename()
        if os.path.exists(path):
            logging.log(NOTE, path)
            for line in open(path):
                sys.stdout.write(line)
            sys.stdout.write("\n")
        else:
            logging.log(NOTE, "No log file %s (job may not have run yet)")
    complete_log = _autocomplete_run_name

    def do_submit(self, line):
        """Submit this job to a run queue."""
        runs = self.parse_runs(line)
        names = [run.name for run in runs]
        for name in names:
            try:
                self.campaign.launch(name)
            except errors.WrongStatus:
                logging.error("This job is in the wrong state to run: {}".format(run.status))
                break
    complete_submit = _autocomplete_run_name

    def do_reset(self, line):
        runs = self.parse_runs(line)
        for run in runs:
            self.campaign.reset(run.name)

    complete_reset = _autocomplete_run_name

    def do_invalidate(self, line):
        runs = self.parse_runs(line)
        for run in runs:
            run.invalidate()

    complete_invalidate = _autocomplete_run_name

    def file_info_table(self, dirname, recurse):
        data = self.file_info_table_sub(dirname, recurse)
        n = len(dirname)+1
        for row in data:
            row[0] = row[0][n:]
        return data

    def file_info_table_sub(self, dirname, recurse):
        data = []
        files = os.listdir(dirname)
        for filename in files:
            path = os.path.join(dirname, filename)
            if os.path.isdir(path) and recurse:
                data += self.file_info_table_sub(os.path.join(dirname, filename), recurse)
            elif os.path.isdir(path):
                st = os.stat(path)
                date = datetime.datetime.fromtimestamp(st.st_mtime)
                date = humanize.naturaltime(date)
                size = humanize.naturalsize(st.st_size)
                lines = "(dir)"
                data.append([path, date, size, lines])
            else:
                st = os.stat(path)
                date = datetime.datetime.fromtimestamp(st.st_mtime)
                date = humanize.naturaltime(date)
                size = humanize.naturalsize(st.st_size)
                lines = utils.line_count(path)
                data.append([path, date, size, lines])
        return data

    def directory_file_list(self, dirname, recurse):
        if not os.path.exists(dirname):
            logging.log(NOTE, "Run directory does not yet exist: %s", dirname)
            return

        files = os.listdir(dirname)
        header = ["Filename",  "Date", "Size", "Lines"]
        data = self.file_info_table(dirname, recurse)
        self.print_table(data, header)


    def do_outputs(self, line):
        run = self.parse_run(line)
        outputs_dir = run.output_directory()
        self.directory_file_list(outputs_dir, True)
    complete_outputs = _autocomplete_run_name


    def do_ls(self, line):
        if line.strip()=="":
            return self.do_list(line)
        run = self.parse_run(line)
        run_dir =run.run_directory()
        self.directory_file_list(run_dir, False)
    complete_ls = _autocomplete_run_name

    def do_lsl(self, line):
        if line.strip()=="":
            return self.do_list(line)
        run = self.parse_run(line)
        run_dir =run.run_directory()
        self.directory_file_list(run_dir, True)
    complete_lsl = _autocomplete_run_name

    def parse_files(self, line, msg):
        words = line.split()
        if len(words)<2:
            logging.error("Syntax: {} run_name filename[s]".format(msg))
            return
        run = self.parse_run(words[0])
        run_dir = run.run_directory()
        files = words[1:]
        paths = []
        for filename in files:
            logging.log(NOTE, filename)
            path = os.path.join(run_dir, filename)
            paths.append(path)
        return paths

    def do_cat(self, line):
        for path in self.parse_files(line, 'cat'):
            if not os.path.exists(path):
                logging.error("File %s does not exist", path)
                continue
            for line in open(path):
                sys.stdout.write(line)
            sys.stdout.write("\n")

    def complete_cat(self, text, line, begidx, endidx):
        words = line.split()
        if (len(words)==2 and line[-1]!=' ') or (words==['cat']):
            return self._autocomplete_run_name(text, line, begidx, endidx)
        else:
            try:
                run = self.parse_run(words[1])
                if len(words)>2:
                    start = words[-1]
                else:
                    start = ""
                files = self._autocomplete_files_in(run.run_directory(), start)
                return files
            except UIError:
                return []

    def do_edit(self, line):
        words = line.split()
        try:
            run_name = words[0]
            filename = words[1]
        except IndexError:
            raise UIError("Syntax: edit run_name file_name")
            return
        try:
            self.campaign.edit(run_name, filename)
        except KeyError:
            raise UIError("Run {} does not exist".format(run_name))
        except WrongStatus as error:
            if error.message==STATUS_NEW:
                raise UIError("Please 'prepare' {} before editing files.".format(run_name))
            else:
                raise UIError("You cannot edit run {} as you have already launched it".format(run_name))
        except ValueError as error:
            logging.error(error)
    complete_edit = complete_cat

    def do_next(self, line):
        run = self.parse_run(line)
        self.campaign.next_version(run.name)
    complete_next = _autocomplete_run_name

    def do_test(self, line):
        """Exercise the syntactic validity of the configuration files by substituting the test sampler and attempting to run that job."""
        run = self.parse_run(line)
        run.runs = self.campaign.runs
        run.test_synchronously()
    complete_next = _autocomplete_run_name

    def do_print(self, line):
        runs = self.parse_runs(line)
        for run in runs:
            table = run.detail()
            self.print_table(table)
            print
            print
    complete_print = _autocomplete_run_name

    def do_clone(self, line, re_parent = False):
        """Make an identical copy of a run, including the parent specification."""
        words = line.split()
        if len(words)!=2:
            raise UIError("Syntax: clone/spawn  run_name  new_run_name")
        run_name, new_run_name = words
        self.campaign.copy(run_name, new_run_name, re_parent)
    complete_clone = _autocomplete_run_name


    def do_spawn(self, line):
        """Make a new run using the old one as parent: the parent parameters are inherited by the child until they are overridden."""
        self.do_clone(line, True)
    complete_spawn = _autocomplete_run_name
    
    

    def do_postprocess(self, line):
        """Postprocess the results of this run line, to produce the final _output_ files."""
        run = self.parse_run(line)
        try:
            run.postprocess_synchronously()
        except WrongStatus:
            logging.error("Run %s is not ready to postprocess (%s)", run.name, run.status)
    complete_postprocess = _autocomplete_run_name

    def do_delete(self, line):
        """Delete a run (the data will be moved to the trash folder)."""
        runs = self.parse_runs(line)
        for run in runs:
            self.campaign.delete(run.name)
    complete_delete = _autocomplete_run_name

    def do_rm(self, line):
        self.do_delete(line)
    complete_rm = complete_delete

    def do_exit(self, line):
        """Exit this application and return to the shell."""
        quit()
        
    def do_quit(self, line):
        quit()

    def emptyline(self):
        pass


parser = argparse.ArgumentParser()
parser.add_argument("campaign_directory", help="The campaign directory to use")
if __name__ == '__main__':
    args = parser.parse_args()
    ui = CampaignUI(args.campaign_directory)
    ui.cmdloop()
