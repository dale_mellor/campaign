#coding: utf-8

# Status values for a run

    # A run in base state is intended only to be used as the base of other runs;
    # it is not itself runnable and will always be editable.
STATUS_BASE           = "BASE"

STATUS_NEW            = "NEW"
STATUS_WAITING        = "WAITING"
STATUS_PREPARED       = "PREPARED"
STATUS_MARKED_INVALID = "MARKED_INVALID"
STATUS_RUNNING        = "RUNNING"
STATUS_QUEUED         = "QUEUED"
STATUS_COMPLETE       = "COMPLETE"
STATUS_ERROR          = "ERROR"
STATUS_SUPERSEDED     = "SUPERSEDED"
STATUS_UNKNOWN        = "UNKNOWN"



STATUS_ICON = {
    STATUS_BASE:           u"\u2602",  # ☂
    STATUS_NEW:            u"",
    STATUS_QUEUED:         u"\u270B",  # ✋
    STATUS_PREPARED:       u"\u2691",  # ⚑
    STATUS_MARKED_INVALID: u"\u2A02" , # ⨂
    STATUS_RUNNING:        u"\u26A1",  # ⚡
    STATUS_WAITING:        u"\u231B",  # ⌛
    STATUS_COMPLETE:       u"\u2705",  # ✅
    STATUS_ERROR:          u"\u274C",  # ❌
    STATUS_SUPERSEDED:     u"\u2A01",  # ⨁
    STATUS_UNKNOWN:        u"\u2753",  # ❓
}



if __name__ == '__main__':
    for name, icon in STATUS_ICON.items():
        print name, icon
