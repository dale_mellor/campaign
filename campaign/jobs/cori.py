from .slurm import SlurmLauncher


TEMPLATE_ = """#!/bin/bash -l

#SBATCH --time={hours}:{minutes}:00

#SBATCH --nodes={nodes}
#SBATCH --ntasks-per-node=32
#SBATCH --cpus-per-task=2

#SBATCH --partition=debug
#SBATCH --license=SCRATCH,project
#SBATCH --constraint=haswell
#SBATCH --job-name=cosmosis

#SBATCH --workdir={working_directory}
#SBATCH --error={working_directory}/{log_name}
#SBATCH --output={working_directory}/{log_name}

#SBATCH --export=ALL

cd {working_directory}
{setup}
srun -n {total_cores} {command_line}
{exit_cmd}
"""


CORES_PER_NODE = 32


class CoriLauncher (SlurmLauncher):

    mpi = True

    def __init__ (self):
        super (CoriLauncher, self)  .  __init__ (
                                               "", TEMPLATE_, CORES_PER_NODE)
