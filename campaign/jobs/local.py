# -*- coding: utf-8 -*-

import collections
import logging
import os
import subprocess
import threading
import time

from ..errors import *
from ..log    import NOTE, JOB, logger
from ..names  import *
from ..status import *



def none_ ():
     return None


class LocalJob (object):


    def __init__ (self, name, command_line):
        self.name         = name
        self.command_line = command_line
        self.id           = None
        self.process      = None
        self.returncode   = None
        self.output       = None



    def __reduce__ (self):
        return (none_, tuple())


    def start(self):

        logger.log (JOB, 
                    "Local launcher starting {}".format (self.command_line))

        self.process = subprocess.Popen (self.command_line,
                                         shell=True,
                                         executable='/bin/bash')
        self.id = self.process.pid


    # Not sure this is needed anymore.
    def results (self):
        if self.poll () is None:
            raise ValueError ("Job is not yet complete.")

        # Allow calling LocalJob.results() multiple times and getting the same
        # answer back.
        if self.returncode is None:
            self.returncode = self.process.returncode
            self.output, _  = self.process.communicate ()

        return self.returncode, self.output


    def status (self):
        if self.process is None:
            return STATUS_QUEUED

        p = self.process.poll ()

        if p is None:
            return STATUS_RUNNING
        elif p==0:
            return STATUS_COMPLETE
        else:
            return STATUS_ERROR


    def poll (self):
        if self.process is None:
            #Job not yet started
            return None
        return self.process.poll ()


    def wait (self):
        if self.process is None:
            #Job not yet started
            return None
        return self.process.wait ()


    def kill (self):
        if self.process is not None:
            self.process.kill ()



class QueueUpdateThread (threading.Thread):

    def __init__ (self, queue, interval, should_stop):
        super (QueueUpdateThread, self)  .  __init__ ()

        self.queue       = queue
        self.interval    = interval
        self.should_stop = should_stop
        self.daemon      = True # Means it gets killed when we kill the
                                # main program.  DM: Not as I understand
                                # it.  It means it does *not* get killed
                                # when we exit the main program, but runs
                                # on as a true system daemon.


    def run (self):
        while not self.should_stop.isSet ():
            time.sleep (self.interval)
            self.queue.update ()
        print "Ending monitor thread"



class LocalJobQueue (object):

    # Must manually call the update command.
    def __init__ (self, max_jobs=1):

        self.completed          = collections.deque ()
        self.running            = collections.deque ()
        self.waiting            = collections.deque ()
        self.max_jobs           = max_jobs
        self.running            = []
        self.update_should_stop = threading.Event ()
        self.lock               = threading.RLock ()

        self.update_thread = QueueUpdateThread (
                                  self,
                                  2.0, # Update interval, seconds.
                                  self.update_should_stop)

        self.update_thread.start ()


    def __reduce__ (self):
        #This is for pickling - just make a new queue.
        return (LocalJobQueue, (self.max_jobs,))


    def close (self):
        self.update_should_stop.set ()


    def append (self, job):
        with self.lock:
            self.waiting.append (job)


    # Called at regular intervals (i.e. 5 seconds) by QueueUpdateThread.
    def update (self):
        with self.lock:
            # Check all running jobs to see if they are complete.
            completed_jobs = []
            for job in self.running:
                if job.poll () is not None:
                    logging.log  (JOB, "Completed local job `%s'.", job.name)
                    completed_jobs.append (job)

            # Move from the running list to the completed list.
            for job in completed_jobs:
                self.running.remove (job)
                self.completed.append (job)

            # Now add more jobs from queue to running.
            while   len (self.running) < self.max_jobs   and   self.waiting:
                job  =  self.waiting.popleft ()
                self.running.append (job)
                logging.log (JOB, "Starting local job `%s'.", job.name)
                job.start ()


    def popCompleted (self):
        with self.lock:
            completed = list (self.completed)
            self.completed.clear ()
            return completed



class LocalLauncher (object):

    mpi = False


    def __init__ (self, max_jobs = 1):
        self.queue     = LocalJobQueue (max_jobs)
        self.job_index = 1



    def launch (self,
                working_directory,
                name,
                command_line,
                exit_file = False,
            # The rest of the options complete the launcher interface, but are
            # not used here.
                nodes = 0,
                time_hours = 0,
                time_minutes = 0,
                script_kwargs = {}):

        submit_command = "cd {}; {} &> {}".format (working_directory,
                                                   command_line,
                                                   JOB_LOG_FILENAME)

        if exit_file:
            submit_command += "; echo $? > " + EXIT_STATUS_FILENAME

        job  =  LocalJob (name, submit_command)

        self.queue.append (job)

        return job



    def update (self):
        self.queue.update ()
