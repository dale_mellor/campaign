from .local import LocalLauncher
from .pbs import PBSLauncher
from .fornax import FornaxLauncher
from .slurm import SlurmLauncher
from .cori import CoriLauncher
