from .pbs import PBSLauncher

FORNAX_TEMPLATE = """
#PBS -l nodes={nodes}:ppn={cores_per_node}
#PBS -l walltime={hours}:{minutes}:00
#PBS -N {name}
#PBS -o {working_directory}/{log_name}
#PBS -j oe

cd {working_directory}
{setup}
mpirun -n {total_cores} {command_line}
{exit_cmd}
"""

FORNAX_CORES_PER_NODE = 32

FORNAX_SETUP = "pushd ~/cosmosis; source ~mellor/cosmosis-joe-setup.sh; popd"



class FornaxLauncher (PBSLauncher):

    mpi = True

    def __init__ (self):
        super (FornaxLauncher, self)  .  __init__ (FORNAX_SETUP,
                                                   FORNAX_TEMPLATE,
                                                   FORNAX_CORES_PER_NODE)
