import os
import logging
import subprocess

from ..errors import *
from ..log    import NOTE, JOB
from ..names  import *
from ..status import *



class SlurmJob (object):


    def __init__ (self, name, command_line):
        self.name         = name
        self.command_line = command_line
        self.id           = None


    def submit(self):

        submission  =  subprocess.Popen (self.command_line,
                                         shell = True, 
                                         stdout = subprocess.PIPE,
                                         stderr = subprocess.STDOUT)

        output, _   =  submission.communicate ()
        status      =  submission.returncode

        if status:
            raise LaunchError(
                   "Could not run submission command `{}' (return code {})"
                                            . format(self.command_line, status))

        self.id   =   output  .  strip ()  .  split (' ') [-1]



    def status (self):
        if self.id is None:
            return None   # Job not yet queued.

        #  Return into \c output a code indicating the status of the job:
        #  |----+--------------------|
        #  | PD | Pending            |
        #  | R  | Running            |
        #  | CA | Cancelled          |
        #  | CF | Configuring        |
        #  | CG | Completing         |
        #  | CD | Completed          |
        #  | F  | Failed             |
        #  | TO | Timeout            |
        #  | NF | Node failure       |
        #  | SE | Special exit state |
        #  |----+--------------------|

        qstat  =  subprocess.Popen ("squeue --noheader --format=%t --jobs={}"
                                                           .format (self.id),
                                    shell=True, 
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)

        output, _  = qstat.communicate ()

        return { 'PD': STATUS_QUEUED,
                 'R':  STATUS_RUNNING,
                 'CG': STATUS_RUNNING,
                 '':   STATUS_COMPLETE,
                 'CD': STATUS_COMPLETE } . get (output.strip (), STATUS_UNKNOWN)



    def poll(self):
        if self.id is None:
            return None

        # This is supposed to mirror Subprocess.poll, which returns the exit
        # status of the command if it is finished or None otherwise.
        if self.status() == STATUS_COMPLETE:
            return 0
        else:
            return None



    def wait (self):
        # Not a good idea!
        return None



    def kill (self):

        if self.id is None:
            return

        subprocess.Popen ("scancel {}".format(self.id),
                          shell = True, 
                          stdout = subprocess.PIPE,
                          stderr = subprocess.STDOUT) . communicate ()
        


class SlurmLauncher (object):

    mpi = True

    def __init__ (self, setup, template, cores_per_node):
        self.setup = setup
        self.template = template
        self.cores_per_node = cores_per_node


    def launch (self,
                working_directory, name, command_line,
                nodes = 1,
                time_hours = 96,
                time_minutes = 0,
                exit_file = False,
                script_kwargs = {}):

        if nodes is None:
            nodes = 1

        if time_hours is None:
            time_hours = 96

        working_directory = os.path.abspath (working_directory)

        if exit_file:
            exit_cmd = "echo $? > " + EXIT_STATUS_FILENAME
        else:
            exit_cmd = ""

        script_args = {
            "nodes"             : nodes,
            "cores_per_node"    : self.cores_per_node,
            "hours"             : int (time_hours),
            "minutes"           : int (time_minutes),
            "name"              : name,
            "working_directory" : working_directory,
            "log_name"          : JOB_LOG_FILENAME,
            "setup"             : self.setup,
            "total_cores"       : int (nodes) * int (self.cores_per_node),
            "command_line"      : command_line,
            "exit_cmd"          : exit_cmd,
        }

        script_args.update (script_kwargs)
        script  =  self.template.format (**script_args)
        script_filename  =  os.path.join (working_directory, JOB_LAUNCH_SCRIPT)

        with open (script_filename, "w") as outfile:
            outfile.write (script)

        logging.log (JOB, "Submitting Slurm job `{}'.".format (script_filename))

        job   =   SlurmJob (name,  "sbatch " + script_filename)

        job.submit ()

        return job



    def re_launch  (self,  job_id,  working_directory,  name):

        job  =  SlurmJob (
                  name,
                  "sbatch " + os.path.join (os.path.abspath (working_directory),
                                            JOB_LAUNCH_SCRIPT))

        job.id = job_id

        return job
