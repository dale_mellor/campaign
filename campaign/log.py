import logging

logger = logging.getLogger("campaign")

#Set the default log format
logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

#The default python log levels are called things like "WARNING", and "ERROR".
#Those are not appropriate here.
NOTE=logging.WARNING
logging.addLevelName("NOTE", NOTE)

JOB=logging.ERROR
logging.addLevelName("JOB", JOB)

logger.setLevel(logging.DEBUG)
