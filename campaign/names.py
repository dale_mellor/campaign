#Standard directory path names used for each run
RUNS_SUBDIR          = "runs"
TRASH_SUBDIR         = "trash"
OUTPUTS_SUBDIR       = "outputs"
TEST_SUBDIR          = "test"

#Standard file names used for each run
CHAIN_FILENAME       = "chain.txt"
PARAM_FILENAME       = "params.ini"
VALUES_FILENAME      = "values.ini"
PRIORS_FILENAME      = "priors.ini"
JOB_LOG_FILENAME     = "log.txt"
JOB_LAUNCH_SCRIPT    = "launch.sub"
EXIT_STATUS_FILENAME = "exit-status.txt"
STATUS_FILENAME      = "status.txt"
