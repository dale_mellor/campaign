import collections
import os
import shutil
import watchdog.events
import watchdog.observers

from .        import utils
from .errors  import WrongStatus, RunExists
from .log     import NOTE, logger
from .names   import *
from .params  import ParameterFile  # For MissingParent exception.
from .run     import Run
from .status  import *



class FileChangeHandler (watchdog.events.FileSystemEventHandler):

    def __init__ (self, campaign):
        super (FileChangeHandler, self).__init__ ()
        self.campaign = campaign

    def on_modified (self, event):
        if not event.is_directory:
            dirname, filename  =  os.path.split (event.src_path)
            if filename in ['params.ini', 'values.ini', 'priors.ini']:
                try:
                    run_name  =  dirname.split (os.path.sep) [-2]
                except IndexError:
                    return
                self.campaign.run_was_edited (run_name)



class Campaign (object):

    def __init__ (self,  campaign_directory,  runs = None,  old_runs = None):

        self.campaign_directory  =  campaign_directory

        self.runs_subdir  =  os.path.join (campaign_directory, RUNS_SUBDIR)
        
        self.runs  =  collections.OrderedDict ()
        if runs is not None:
            for run in runs:
                self.runs [run.name] = run

        self.old_runs  =  {}
        if old_runs is not None:
            self.old_runs  =  {(run.name, run.version):run for run in old_runs}

        self.monitor_run_directory ()


    
    def monitor_run_directory (self):
        self.observer = watchdog.observers.Observer ()

        self.observer.schedule (FileChangeHandler (self),
                                self.runs_subdir,
                                recursive = True)

        self.observer.start ()

        self.updated_runs = set ()



    def run_was_edited (self, run_name):
        self.updated_runs.add (run_name)


    @classmethod
    def create (cls, directory):
        return cls (directory, [])


    @classmethod
    def load (cls, directory):

        runs_subdir = os.path.join (directory, RUNS_SUBDIR)

        if not os.path.exists (runs_subdir):
            print "Warning: no runs found here."
            return cls (directory)

        runs = []
        old_runs = []
        local_runs = collections.OrderedDict ()
        run_dirs = os.listdir (runs_subdir)

        # We have no guarantee of being able to read all the run
        # directories in the natural order (must have parents' data before
        # we can read their children), so we do what we can and then keep
        # repeating the whole process until all have been successfully
        # read.
        while run_dirs:
            for run_dir_ in run_dirs:
                run_dir = os.path.join (runs_subdir, run_dir_)
                if not os.path.isdir (run_dir):
                    run_dirs.remove (run_dir_)
                    continue
                versions = []

                version_dirs = [os.path.join (run_dir, x) 
                                 for  x  in  os.listdir (run_dir) 
                                 if  os.path.isdir (os.path.join (run_dir, x))]

                for version_dir in version_dirs:
                    version = os.path.split (version_dir) [-1]
                    print "Found version {} of {}".format (version, run_dir)
                    try:
                        run = Run.read_directory (
                                           directory, version_dir, local_runs)
                    except ParameterFile.MissingParent:
                        # This run's data depend on those of another
                        # (parent) run, but we don't have those.  Leave
                        # this for now, and we'll come back to it later
                        # when hopefully the parent has been read.
                        break
                    except IOError as error:
                        logger.error ("Could not read possible run in %s." 
                                                            + "  Error was: %s",
                                      version_dir, error)
                        continue
                    else:
                        versions.append (run)

                if versions:
                    run_dirs.remove (run_dir_)
                    local_runs [versions [-1].name]  =  versions [-1]
                    runs.append (versions [-1])
                    runs  +=  versions [:-1]

        return cls (directory, runs=runs, old_runs=old_runs)



    def runs_with_status (self, status):
        for run in self.runs.values ():
            if run.status == status:
                yield run


    def replace (self, old_run, new_run):
        old_run.supersede ()
        self.old_runs [(old_run.name, old_run.version)] = old_run
        self.runs [new_run.name] = new_run


    def copy (self, existing_name, new_name, re_parent = False,
              keep_state = False):

        assert  existing_name != new_name

        if  new_name  in  self.runs:
            raise RunExists (new_name)

        old_run = self.runs [existing_name]
        new_run = old_run.copy (new_name)

        if keep_state:
            new_run.status = old_run.status

        self.runs [new_name] = new_run

        if re_parent:

            new_run.parameters.params.parent = existing_name
            new_run.parameters.values.parent = existing_name
            new_run.parameters.priors.parent = existing_name

            new_run.make_virgin_config_files ()

            new_run.parameters.read (new_run.run_directory (),
                                     pre_path = os.getcwd (),
                                     runs = self.runs)


        else:

            def c (old, new, base_name):

                old_path = os.path.join (old.run_directory (), base_name)
                
                if os.path.exists (old_path):
                    shutil.copyfile (old_path,
                                     os.path.join (new.run_directory (),
                                                   base_name))

            c (old_run, new_run, PARAM_FILENAME)
            c (old_run, new_run, VALUES_FILENAME)
            c (old_run, new_run, PRIORS_FILENAME)



    def next_version (self, name):
        run = self.runs.pop (name)
        new_run = run.next_version ()
        logger.log (NOTE,
                    "Creating new version %d of `%s'", new_run.version, name)
        self.replace (run, new_run)


    def drop_run (self, name):
        shutil.rmtree (self.runs [name].top_run_directory ())
        self.runs.pop (name)
        logger.log (NOTE, "Deleting run `%s'", name)


    def rename_run (self, old_name, new_name):
        logger.log (NOTE, "Doing run re-name:")
        self.copy (old_name, new_name, keep_state=True)
        self.drop_run (old_name)


    def launch (self, name):
        self.runs [name].launch (self.runs)


    def edit (self, run_name, filename):
        run  =  self.runs [run_name]
        # !! Should check if run has the wrong status - can only edit if
        #    it is not running or already run.
        run.edit (filename)


    def run_names (self):
        return  [run.name  for  run  in  self.runs.values ()]     


    # !!! We ought to be calling this from the HTTP GUI.
    def delete (self, run_name):
        run = self.runs [run_name]
        run_dir = run.run_directory ()
        trash_dir_base  =  os.path.join (self.campaign_directory,
                                         TRASH_SUBDIR,
                                         RUNS_SUBDIR,
                                         run_name,
                                         str (run.version))
        trash_dir = trash_dir_base
        i = 0
        while os.path.exists (trash_dir):
            trash_dir  =  trash_dir_base  +  '.'  +  str (i)
            i += 1
        utils.mkdir (trash_dir)
        shutil.move (run_dir, trash_dir)
        shutil.rmtree ('/'.join (run_dir.split ("/") [:-1]))
        del self.runs [run_name]
        logger.log (NOTE,
                    "Removing run output {} to {}".format (run_name, trash_dir))



    def reset (self, run_name):
        run  =  self.runs [run_name]
        self.delete (run_name)
        run.status  =  STATUS_NEW
        # Recreate it.
        self.runs [run_name]  =  run
        run.prepare ()



    # TODO
    #
    # Understands job dependencies.


# End of class Campaign.
