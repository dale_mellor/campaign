import glob
import logging
import os
import re
import shutil
import subprocess

from .        import jobs, utils
from .errors  import WrongStatus, RunExists
from .log     import NOTE, logger
from .names   import *
from .params  import RunParameters, ParameterFile
from .status  import *

from  cosmosis.runtime.config  import  CosmosisConfigurationError, Inifile


"""
Ways of creating a run
 - from a yaml file
 - by copying an existing one
 - by updating an old one to a new version
 - by loading it from disc
"""


class Run (object):


    def __init__ (self, campaign_directory, name, text, parameters,
                  parents, version):
        self.campaign_directory = campaign_directory
        self.parameters = parameters
        self.name = name
        self.text = text
        if not self.text.endswith ("\n"):
            self.text += "\n"
        self._status = STATUS_NEW
        self.parents = parents
        self.version = version
        self.launch_parameters = {}
        self.postprocess_parameters = {}
        self.job = None



    def top_run_directory (self):
        return os.path.join (self.campaign_directory, RUNS_SUBDIR, self.name)

    def run_directory (self):
        return os.path.join (self.top_run_directory (), str (self.version))

    def chain_path (self):
        return os.path.join (self.run_directory (), CHAIN_FILENAME)

    def output_directory (self):
        return os.path.join (self.run_directory (), OUTPUTS_SUBDIR)

    def test_directory (self):
        return os.path.join (self.run_directory (), TEST_SUBDIR)

    def status_filename (self):
        return os.path.join (self.run_directory (), STATUS_FILENAME)

    def log_filename (self):
        return os.path.join (self.run_directory (), JOB_LOG_FILENAME)



    @classmethod
    def read_directory (cls, campaign_directory, run_directory, runs=None):
        path_bits = run_directory.split (os.path.sep)
        name = path_bits [-2]
        version = int (path_bits [-1])
        parameters = RunParameters.from_ini (
                                          run_directory, PARAM_FILENAME, runs)
        text = parameters.params [("campaign", "text")]

        run = cls (campaign_directory, name, text, parameters, 
                   parameters.parents, version)
        run.read_status_file ()

        return run



    def status ():
        doc = "The status property."
        def fget (self):
            return self._status
        def fset (self, value):
            if value!=STATUS_NEW:
                if not os.path.exists (self.run_directory ()):
                    utils.mkdir (self.run_directory ())
                with   open (self.status_filename (), 'w')   as  out:
                    if   self.job is not None   and   self.job.id is not None:
                        out.write ("J{}" . format (self.job.id))
                    else:
                        out.write ("%s\n"%value)
            self._status = value
        def fdel (self):
            del self._status
        return locals ()

    status = property (**status ())


    def read_status_file (self):
        if os.path.exists (self.status_filename ()):
            in_ = open (self.status_filename ()).read ().strip ()
            if in_ [0] == 'J':
                self._status = STATUS_RUNNING
                self.re_launch (int (in_ [1:]))
            else:
                self._status = in_


    def reread_files (self):
        self.read_status_file ()


    def __str__ (self):
        return "<Run {} v{}>".format (self.name, self.version)


    def __repr__ (self):
        return self.__str__ ()


    def same_parameters (self, other):
        return self.parameters == other.parameters


    def needs_rerun (self, other):
        assert self.name == other.name
        if not self.same_parameters (other):
            logger.log (logging.DEBUG,
                        "Run %s has changed because parameters have changed.",
                        self)
            return True
        if not self.launch_parameters == other.launch_parameters:
            logger.log (logging.DEBUG,
                        "Run %s has changed because launch parameters have "
                                                                  + "changed.",
                        self)
            return True
        return False


    def edit (self, filename):
        if self.status != STATUS_PREPARED:
            raise WrongStatus (self.status)
        run_dir = self.run_directory ()
        path = os.path.join (run_dir, filename)
        if not os.path.exists (path):
            raise ValueError ("No file {} in run {}. ({})"
                                    .format (filename, self.name, path))
        cmd  =  "emacs -nw " + path
        logger.log (NOTE, cmd)
        os.system (cmd)


    @classmethod
    def new_base (cls, campaign_directory, ini_file):

        data = Inifile (ini_file)

        # This will raise CosmosisConfigurationError if the requisite
        # entries in the .ini file are missing.
        ret = cls (campaign_directory,
                   name = re.sub ('.ini$', '', os.path.basename (ini_file)),
                   text = data.get ("campaign", "text", None),
                   # !!!!!
                   parameters = RunParameters (None, None, None, []),
                   parents = [],
                   version = 1)

        # This will create the directory structure under the campaign
        # directory as a side-effect.
        ret.status = STATUS_BASE

        params_file = os.path.join (ret.run_directory (), PARAM_FILENAME)

        shutil.copy (ini_file, params_file)

        ret.parameters.params = ParameterFile (params_file)


        values = data.get ("pipeline", "values", False)

        if values == False:
            ret.parameters.values = ParameterFile (None)
        else:
            values = os.path.join (os.path.dirname (ini_file), values)
            values_file = os.path.join (ret.run_directory (), "values.ini")
            shutil.copy (values, values_file)
            ret.parameters.values = ParameterFile (values_file)


        priors = data.get ("pipeline", "priors", False)

        if priors == False:
            ret.parameters.priors = ParameterFile (None)
        else:
            priors = os.path.join (os.path.dirname (ini_file), priors)
            priors_file = os.path.join (ret.run_directory (), "priors.ini")
            shutil.copy (priors, priors_file)
            ret.parameters.priors = ParameterFile (priors_file)


        return ret

        

    def get_param (self, section, name):
        return self.parameters.params [section, name]
    def get_param_x (self, section, name, default):
        try:
            return self.get_param (section, name)
        except CosmosisConfigurationError:
            return default
    def set_param (self, section, name, value):
        self.parameters.params [section, name] = value
    def del_param (self, section, name):
        del self.parameters.params [section, name]

    def get_value (self, section, name):
        return self.parameters.values [section, name]
    def get_value_x (self, section, name, default):
        try:
            return self.get_value (section, name)
        except CosmosisConfigurationError:
            return default
    def set_value (self, section, name, value):
        self.parameters.values [section, name] = value
    def del_value (self, section, name):
        del self.parameters.values [section, name]

    def get_prior (self, section, name):
        return self.parameters.priors [section, name]
    def get_prior_x (self, section, name, default):
        try:
            return self.get_prior (section, name)
        except CosmosisConfigurationError:
            return default
    def set_prior (self, section, name, value):
        self.parameters.priors [section, name] = value
    def del_prior (self, section, name):
        del self.parameters.priors [section, name]

    def get_sampler (self):
        return self.get_param ('runtime', 'sampler') 

    def set_sampler (self, sampler):
        self.set_param ('runtime', 'sampler', sampler)


    def supersede (self):
        if self.status == STATUS_MARKED_INVALID:
            logger.log (NOTE, 
                        "%s is superseded but it is already marked "
                                       + "invalid so leaving its status alone",
                        self.name)
        else:
            logger.log (NOTE, "Marking %s as superseded", self.name)
            self.status = STATUS_SUPERSEDED
            
            
    def next_version (self):
        run = self.copy (self.name, version=self.version+1)
        return run



    def copy (self, name, version=1, text=None):

        if text is None:
            text = self.text
            if text [:9] != 'Copy of: ':
                text = 'Copy of: ' + text

        run = self.__class__ (self.campaign_directory, name, 
                              text, self.parameters.copy (),
                              self.parents, version=version)

        run.launch_parameters = self.launch_parameters.copy ()

        run.postprocess_parameters = self.postprocess_parameters.copy ()

        run.prepare ()

        return run



    def make_virgin_config_files (self):

        p = RunParameters (ParameterFile (None),
                           ParameterFile (None),
                           ParameterFile (None),
                           [self.parameters.params.parent])

        p.params.base_path = None
        p.values.base_path = None
        p.priors.base_path = None

        p.params.parent = self.parameters.params.parent
        p.values.parent = self.parameters.values.parent
        p.priors.parent = self.parameters.priors.parent

        p.set_paths ()   # Put default values into parameters.

        p.write (self.run_directory ())



    def prepare (self, no_files = False):
        # Check status. Can only prepare new runs, not ones that are already
        # complete.
        if self.status != STATUS_NEW:
            raise WrongStatus (self.status)

        # Make new directory based on name and version.  If it exists already
        # that is a mistake - we need a new version.
        directory = self.run_directory ()
        if os.path.exists (directory):
            raise RunExists (directory)

        logger.log (NOTE, "Preparing run %s in directory %s", 
                    self.name, directory)

        utils.mkdir (directory)

        if not no_files:

            # Save all parameter files to this directory.
            self.parameters.write (directory)

        # Update status.
        self.status = STATUS_PREPARED
        
        #create launch script there too?



    def write_run_files (self, prior_changes = {}):

        run_dir = self.run_directory ()
     
        def wrt (base_name, changes = {}):
            params = ParameterFile (None)

            params.read (run_dir,
                         runs = self.runs,
                         base_name = base_name,
                         pre_path = os.getcwd ())

            for key in changes:
                params [key] = changes [key]

            with open (run_dir + "/__" + base_name, 'w') as out:
                params.write (out)

        a = { ("pipeline", "values"): "__values.ini",
              ("pipeline", "priors"): "__priors.ini" }

        a . update (prior_changes)

        wrt ("params.ini", a)
        wrt ("values.ini")
        wrt ("priors.ini")



    def run_command (self, mpi=False):
        command  =  ["cosmosis",  "__" + PARAM_FILENAME]
        if mpi:
            command.append ("--mpi")
        return command
    


    def _run_process (self, command):

        directory = self.run_directory ()

        logger.log (NOTE, "Running command %s in directory %s", 
                    ' '.join (command), directory)
        
        return subprocess.Popen (command,
                                 stdout=subprocess.PIPE, 
                                 stderr=subprocess.STDOUT,
                                 cwd=directory)



    def test_synchronously (self, pipeline_test=False):

        # First, check if we are already set to use the test sampler. If
        # so, and this is just the pipeline test at the start of a run,
        # then we don't need to run the test twice.
        sampler = self.get_sampler ()
        if pipeline_test and sampler=="test":
            return 0, ""

        # Temporarily set the sampler to the test sampler and run.  We
        # don't tell it to test_first as that would be an infinite
        # recursion.

        # Overwrite the sampler.

        self.write_run_files ({("runtime", "sampler"): "test"})

        logger.log (NOTE, 
                    "Testing the pipeline: switched sampler from '%s' to "
                                                                    + "'test'", 
                    sampler)

        return_code, output = self.run_synchronously (test_first=False,
                                                      testing=True)

        if return_code:
            logger.error ("Pipeline test failed")

        logger.info ("\n"+output)
        logger.log (NOTE, 
                    "Pipeline test successful: switching sampler back "
                                                                   + "to '%s'",
                    sampler)

        if self.status == STATUS_COMPLETE:
            self.status = STATUS_PREPARED

        return return_code, output



    def run_synchronously (self, test_first=True, testing=False):
        #We should only start runs that have been prepared, not
        #been run already, and are not in an error status
        if self.status!=STATUS_PREPARED:
            raise WrongStatus (self.status)

        #If the sampler is not already the test sampler,
        #optionally run a test of the pipeline beforehand
        #to test for problems.
        if test_first:
            test_code, test_output = self.test_synchronously (
                                                          pipeline_test=True)

            #If the test fails 
            if test_code:
                self.status=STATUS_ERROR
                return test_code

        # Get the command we are to run and spawn a process running it.
        if not testing:
            self.write_run_files ()
        process = self._run_process (self.run_command ())

        #Wait for the process to complete and check how it went
        output,_ = process.communicate ()
        return_code = process.returncode

        #Update run status depending on how it went.
        if return_code==0:
            self.status = STATUS_COMPLETE
        else:
            self.status = STATUS_ERROR

        #return the printed output and the return status
        return return_code, output



    def postprocess_synchronously (self, parameters={}, force=False):
        #We usually only want to postprocess complete commands,
        #but occasionally we might want to do it in the middle of 
        #a run in case things partially finished
        if self.status!=STATUS_COMPLETE and not force:
            raise WrongStatus (self.status)

        #Get the command to run, pasing any extra postprocessing
        #requirements in.
        command = self.postprocess_command (parameters)

        #Set up the process and run it
        process=self._run_process (command)

        output,_ = process.communicate ()

        return process.returncode, output



    def output_files (self):
        return glob.glob (os.path.join (self.output_directory (), "*"))

    def test_files (self):
        return glob.glob (os.path.join (self.test_directory (), "*"))

    def postprocess_command (self, parameters={}):
        # Check if status is finished; if not, raise error, unless Force
        # is True.

        # Global postprocess parameters and specific ones for this run
        # (might tweak, e.g. burn-in, etc.) should be dictionaries of
        # "--burn XXX" etc.
        parameters = parameters.copy ()
        parameters.update (self.postprocess_parameters)
        parameter_command_line = sum ((list (s) for s in parameters.items ()),
                                      [])

        # Construct the command that runs postprocess.
        return ["postprocess", 
                   "-o", OUTPUTS_SUBDIR, 
                   "__" + PARAM_FILENAME] + parameter_command_line



    def launch (self,  runs,  test_first = True):

        if self.status != STATUS_PREPARED:
            raise WrongStatus (self.status)

        self.runs = runs

        if test_first:
            # Check that the pipeline is working first.

            test_code, test_output = self.test_synchronously (
                                               pipeline_test = True)

            # If the test fails.
            if test_code:
                self.status = STATUS_ERROR
                logger.error ("Pre-launch test failed. Output below.")
                print test_output
                logger.error ("Pre-launch test failed. Output above.")
                return

        launcher_name = self.get_param_x (
                                    "campaign", "launcher", "local").lower ()

        if   launcher_name  ==  "fornax/pbs":
            launcher  =  jobs.FornaxLauncher ()
        elif launcher_name  ==  "cori/slurm":
            launcher  =  jobs.CoriLauncher ()
        elif launcher_name  ==  "local":
            launcher  =  jobs.LocalLauncher (max_jobs = 1)
        else:
            raise LaunchError (
                   "There was a problem instantiating the `"
                          + launcher_name
                          + "' launcher, please check your params.ini file")


        self.write_run_files ()

        # We have to get rid of this as it is infintely recursive (the runs
        # array includes a pointer to ourself), and causes problems for YAML
        # pickling.
        self.runs = None

        self.job = launcher.launch (
                       self.run_directory (),
                       self.name,
                       " ".join (self.run_command (mpi = launcher.mpi)),
                       exit_file = True,
                       script_kwargs = self.launch_parameters,
                       nodes = self.get_param_x ("campaign", "nodes", None),
                       time_hours = self.get_param_x
                                                  ("campaign", "hours", None),
                       time_minutes = self.get_param_x 
                                                  ("campaign", "minutes", 0)
                   )

        self.status = STATUS_QUEUED



    def re_launch (self, job_id):

        launcher_name = self.get_param_x (
                                    "campaign", "launcher", "local").lower ()

        if   launcher_name  ==  "fornax/pbs":
            launcher  =  jobs.FornaxLauncher ()
        elif launcher_name  ==  "cori/slurm":
            launcher  =  jobs.CoriLauncher ()
        elif launcher_name  ==  "local":
            launcher  =  jobs.LocalLauncher (max_jobs = 1)
        else:
            raise LaunchError (
                   "There was a problem instantiating the `"
                          + launcher_name
                          + "' launcher, please check your params.ini file")


        self.job = launcher.re_launch (job_id,
                                       self.run_directory (),
                                       self.name)

        self.status = STATUS_QUEUED



    def invalidate (self):
        if self.job is not None:
            self.job.kill ()
            self.job = None
        self.status = STATUS_MARKED_INVALID



    def exit_status (self):
        exit_filename = os.path.join (self.run_directory (),
                                      EXIT_STATUS_FILENAME)
        status = None
        if os.path.exists (exit_filename):
            status = open (exit_filename).read ().strip ()
            if status=="0":
                status = STATUS_COMPLETE
            else:
                status = STATUS_ERROR
        return status



    def update (self):
        if self.job is not None:
            status = self.job.status ()
            if status == STATUS_COMPLETE:
                # The job status being complete just tells us that the the
                # job finished, not that it finished successfully.
                status = self.exit_status ()

                # If we are complete then we can delete the job.
                self.job = None

        # Or we may not have a job at all.  Either we have never run at
        # all or we have run and finished earlier.
        else:
            if self.status == STATUS_COMPLETE:
                status = self.exit_status ()
            else:
                status = None
            # If this is None then the job has not run at all: status
            # should be STATUS_NEW, so no changes.
            if status is None:
                # If we have no exit status file telling us the output
                # then the job has not run and we have no new updates.
                return
        if status != self.status:
            self.status = status



    def detail (self):
        table = [("Name", self.name),
                 ("Version", self.version),
                 ("Info", self.text),
                 ("Status", STATUS_ICON [self.status] + " " + self.status),
                 ("Parents", ','.join (self.parents)),
                 ("Base Params", self.parameters.params.base_path), 
                 ("Base Values", self.parameters.values.base_path), 
                 ("Base Priors", self.parameters.priors.base_path)]
        for name,p in [("Params", self.parameters.params),
                       ("Values", self.parameters.values), 
                       ("Priors", self.parameters.priors)]:
            if p.changes:
                for i,line in enumerate (utils.changes_text (p.changes)):
                    line = line [:50]
                    if len (line)==50:
                        line += " ... "
                    if i==0:
                        table.append ((name, line))
                    else:
                        table.append (("", line))
            else:
                table.append ((name, ""))
        return table



    def info (self):
        self.update ()
        chain = self.chain_path ()
        if os.path.exists (chain):
            lines = utils.uncommented_line_count (chain)
        else:
            lines = -1
        try:
            sampler = self.get_sampler ()
        except CosmosisConfigurationError:
            sampler = "?"

        job = getattr (self.job, 'id', '')

        outputs = self.output_files ()
        n_output = len (outputs)

        return (self.name, self.version, self.status, sampler, lines, n_output,
                job)

    # Update if we update info method:
    INFO_FIELDS = ("Name", "Version", "Status", "Sampler", "Lines", "Outputs",
                   "Job")
