from .names import *
from cosmosis.runtime.config import IncludingConfigParser, Inifile, CosmosisConfigurationError
import contextlib
import cStringIO
import logging
import os
import re


DEFAULT_PARAMETERS = {
    ('pipeline' , 'values'   ): VALUES_FILENAME,
    ('pipeline' , 'priors'   ): PRIORS_FILENAME,
    ('test'     , 'save_dir' ): TEST_SUBDIR,
    ('output'   , 'filename' ): CHAIN_FILENAME,
    ('output'   , 'format'   ): 'text',
}



parent_re = re.compile (r"^%parent (.*)$")



class ParameterFile (object):

    class MissingParent(Exception):
        pass

    def __init__(self, base_path):
        if base_path is None or base_path is False:
            self.base_path = None
        else:
            self.base_path = os.path.abspath(base_path) #path to base object
        self.changes = {}
        self._base_ini = None
        self.parents = []


    def __eq__ (self, other):
        try:
            return ((self.changes == other.changes)
                       and   (self.base_path == other.base_path))
        except AttributeError:
            return False


    def __ne__ (self, other):
        return not __eq__ (self, other)


    def differences (self, other):
        diffs = {}

        for key, val1 in self.changes.items():
            val2  =  other.changes.get (key)
            if val1 != val2:
                diffs [key]  =  (val1, val2)

        # And the reverse too.  Will re-write but never mind.
        for key, val2 in other.changes.items ():
            val1  =  self.changes.get (key)
            if val1 != val2:
                diffs [key]  =  (val1, val2)

        return diffs


    @property
    def base_ini(self):
        if self._base_ini is None:
            self._base_ini = Inifile(self.base_path)
        return self._base_ini


    @base_ini.deleter
    def base_ini(self):
        self._base_ini = None


    def parent_item(self, parameter):
        section,name=parameter
        return self.base_ini.get(section, name, default=False)


    @contextlib.contextmanager
    def temporary_value(self, section, name, value):
        old_value = self.changes.get((section,name))
        self[(section,name)] = value
        yield
        if old_value is None:
            del self[(section,name)]
        else:
            self[(section,name)] = old_value


    def __setitem__(self, parameter, value):
        section, name = parameter
        self.changes[(section,name)] = value


    def __delitem__(self, parameter):
        del self.changes[parameter]


    def __getitem__(self, parameter):
        item  =  self.changes.get (parameter)
        if item is not None:
            return item
        item  =  self.parent_item (parameter)
        if item is not None:
            return item
        section, name  =  parameter
        raise KeyError ("No parameter `" + name
                                          + "' in section `" + section + "'.")


    def __contains__(self, parameter):
        return parameter in self.changes


    def copy (self):
        result = self.__class__ (self.base_path)
        result.changes.update (self.changes)
        return result


    def write (self, stream):
        """
        Write the parameter file in abbreviated form, where the base ini
        file is included in the parent by reference - the cosmosis %include 
        notation is used at the top of the file to tell cosmosis to include it
        first.
        """

        # First the include statement, to contain the base.  This may be blank -
        # if so, ignore.
        if self.base_path is not None:
            stream.write ("%include " + self.base_path + "\n\n")

        if   hasattr (self, 'parent')  and  self.parent is not None:
            stream.write ("%parent " + self.parent + "\n\n")

        # We use the standard ini file mechanism to save the parameters.
        # Because we have already just included the parent base file we do not
        # need to include it again- hence the "None" argument.
        Inifile (None, override=self.changes)  .  write (stream)



    # Part of the implementation of the following function.
    @staticmethod    
    def expand_parents (runs, path, base_name, params):

        parents = None

        ret = cStringIO.StringIO ()
        f = os.path.join (path, base_name)

        try:
            with open (f, 'r') as input:
                while True:

                    line = input.readline ()
                    if not line:
                        break

                    match = parent_re.match (line)

                    if match:
                        if match.group (1) in runs:

                            ParameterFile.expand_parents (
                                   runs,
                                   runs [match.group (1)].run_directory (),
                                   base_name,
                                   params)

                            if parents is None:
                                parents = [match.group (1)]

                        else:
                            raise ParameterFile.MissingParent

                    else:
                        ret.write (line)

            ret.seek (0)
            params._read (ret, path)

        except IOError as e:
            # This is not really a problem.
            pass
            # sys.stderr.write ("Failed to open file `" + f + "': "
            #                     + e.strerror + "\n")

        if parents is None:
            return []
        else:
            return parents




    # If runs are provided, then all runs in %parent lines will be read to get a
    # full set of working parameters.

    def read (self, path, runs = None, base_name = None, pre_path = None):

        self.changes = {}
        ini = IncludingConfigParser ()

        if runs is not None:
            self.parents = ParameterFile.expand_parents (
                                                   runs, path, base_name, ini)

        else:
            ini.no_expand_includes = True
            ini.read (path)

        for section in ini.sections ():
            for name, value in ini.items (section):
                self.changes [(section, name)] = value



    def write_full (self, stream):
        """
        Write the parameter file in full form, by loading the base ini file
        and writing it out in full. This would typically be used for debugging.
        """
        # Use the standard inifile mechanism to load the file and apply the
        # changes
        Inifile (self.base_path, override=self.changes)  .  write (stream)



class RunParameters(object):

    def __init__(self, params, values, priors, parents):
        #ParameterFile instances
        self.params = params
        self.values = values
        self.priors = priors
        self.parents = parents


    def __eq__(self, other):

        try:
            param_diffs = self.params.differences(other.params)
            value_diffs = self.values.differences(other.values)
            prior_diffs = self.priors.differences(other.priors)
        except AttributeError:
            return False

        #remove the default ones which get written always - we ignore
        #these
        for key in DEFAULT_PARAMETERS:
            param_diffs.pop(key, None)

        if param_diffs:
            logger.log(logging.DEBUG, "Parameter file has changed (%s).",
                param_diffs.keys())
            return False
        if value_diffs:
            logger.log (logging.DEBUG,
                        "Values file has changed (%s).",
                        value_diffs.keys())
            return False
        if self.priors!=other.priors:
            logger.log (logging.DEBUG,
                        "Priors file has changed (%s).",
                        prior_diffs.keys())
            return False
        return True



    def copy (self):
        return RunParameters (self.params.copy(),
                              self.values.copy(),
                              self.priors.copy(),
                              self.parents)



    def set_paths (self):
        for (section,key), value in DEFAULT_PARAMETERS.items():
            self.params[section, key] = value
 


    def write (self, directory):

        def open_ (filename):
            return open (os.path.join (directory, filename), "w")

        with open_ (PARAM_FILENAME) as out:
            self.params.write (out)
        with open_ (VALUES_FILENAME) as out:
            self.values.write (out)
        with open_ (PRIORS_FILENAME) as out:
            self.priors.write (out)



    def read (self, directory, runs = None, pre_path = None):

        def r (file, file_name):
            file.read (directory,
                       runs = runs,
                       base_name = file_name,
                       pre_path = pre_path)

        r (self.params, PARAM_FILENAME)
        r (self.values, VALUES_FILENAME)
        r (self.priors, PRIORS_FILENAME)



    @classmethod
    def from_ini (cls, base_dir, params_path, runs = None):

        def path (file):
            return os.path.join (base_dir, file)

        params_name = path (params_path)

        params = ParameterFile (params_name)
        params.read (base_dir, runs=runs, base_name=params_path)

        #priors file is optional
        priors_name = params [("pipeline", "priors")]

        if priors_name is not False:
            priors_name = path (priors_name)

        return cls (params,
                    ParameterFile (path (params [("pipeline", "values")])),
                    ParameterFile (priors_name),
                    params.parents)
