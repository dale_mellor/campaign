from campaign         import Campaign
from campaign         import jobs
from pyramid.config   import Configurator
from pyramid.settings import asbool
import os


def main (global_config, **settings):
    """The pyramid framework (a.k.a. the `pserve' program) looks for this method
       in this file to get the ball rolling.  This function returns a Pyramid
       WSGI application, built through facilities provided by the pyramid
       framework.  The application includes a Campaign object which is our
       access to the Campaign Manager engine."""

    # Create the campaign object by loading from an existing directory.
    settings ['campaign'] = Campaign.load (settings ['campaign.directory'])

    # Configure our web application: associate web URLs with methods defined in
    # server.py.
    config  =  Configurator (settings=settings)
    config.include         ('pyramid_chameleon')
    config.add_static_view ('static', '', cache_max_age=3600)
    config.add_route       ('home', '/')
    config.add_route       ('get-campaign-state', '/campaign-state')
    config.add_route       ('get-png', '/png')
    config.add_route       ('provide-text', '/file-edit')
    config.scan            ()

    # Make our web application.
    return config.make_wsgi_app ()
